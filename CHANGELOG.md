# Changelog
## v 1.7.0
- add the ability to show the node ids in the upper left corder of a node

## v 1.6.1
- fix editor to enable ghost edges

## v 1.6
- Fix transparency in certain svg viewers (e.g Inkscape was showing fill as black when fill="transparent", so now it's fill="none" for things that are supposed to not have fill)
- add the ability to specify "ghost edges" i.e. invisible edges. This can help with better aligning graphs to the author's liking.

## v 1.5
- Add ability to chain nodes together in one line, e.g. node1->node2->node3

## v 1.4
- Add ability to specify node-level options
- Add cornerRadius option

## v 1.3
- Add autoWrapLength option, where you can specify a character length to word-wrap at

## v 1.2
- Clusters can now specify options at the cluster level
- They will also inherit and override options from parent clusters or from the root graph

## v 1.1.2
- Added support for blank lines
- Fixed node sizing bug in firefox

## v 1.1.0

- Support for nested clustering added. 
- bugfix in string parser: later usage of same node without including label/url was overwriting the label/url
- made cluster labels optional in the string parser