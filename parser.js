/**
 * @license
 * dagre-svg parser
 * 
 * ISC License
 * Copyright (c) 2021 Andrew Ulrich
 * 
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

 /**
  * See the readme for the string format to pass in
  * @param {*} inputString
  */
function parseToDiagram(inputString) {
  ///////////////////////////////internal functions//////////////////
  inputString=inputString.trim()
  function stripUndefinedValues(obj) {
    Object.keys(obj).forEach(key => obj[key] === undefined && delete obj[key])
  }
  function stripBrackets(str) {
    if(typeof str != 'string') return str
    const trimmed=str.trim()
    return trimmed.substring(1,trimmed.length-1).replace(/\\n/g,'\n')
  }
  function compileLines(lineInfo) {
    const nodeArray=lineInfo
      .filter(n=>n.type !='option' && n.type !='cluster' && n.type !='nodeOptions')
      .reduce((accum,n)=>[...accum,...n.nodes],[]) //flatten the array
      .reduce((accum,node)=>{
        //modify accumulated nodes instead of duplicating/replacing them
        const existing=accum.find(needle=>needle.id==node.id)
        if(existing) {
          existing.label = existing.label || node.label
          existing.url = existing.url || node.url
        } else {
          accum.push(node)
        }
        return accum
      },[])
    
    const nodes=nodeArray.reduce((accum,n)=>({...accum,[n.id]:n.label || n.id}),{})
    const urls=nodeArray.reduce((accum,n)=>({...accum,[n.id]:n.url}),{})
    stripUndefinedValues(urls)
    const edges=lineInfo
      .filter(n=>n.type !='option' && n.type !='cluster' && n.type !='nodeOptions' && n.edges)
      .map(n=>n.edges)
      .reduce((accum,curr)=>accum.concat(curr),[])

    const ghostEdgeFlags=lineInfo
    .filter(n=>n.type !='option' && n.type !='cluster' && n.type !='nodeOptions' && n.edges)
    .map(n=>n.ghostEdgeFlags)
    .reduce((accum,curr)=>accum.concat(curr),[])
    
    const allParserOpts=['autoWrapLength','margin','nodeColor','nodeBorder','edgeColor','boldFirstLine','hasArrows','edgeWidth','arrowSize','edgeOpacity','clusterLabelPos','showIds']
    const parserOpts=lineInfo
      .filter(o=>o.type=='option')
      .filter(o=>allParserOpts.includes(o.key))
      .reduce((accum,curr)=>({
        ...accum,
        [curr.key]:unStringValue(curr.value)
      }),{})
    const graphOpts=lineInfo
    .filter(o=>o.type=='option')
    .filter(o=>!allParserOpts.includes(o.key))
    .reduce((accum,curr)=>({
      ...accum,
      [curr.key]:unStringValue(curr.value)
    }),{})
    let nodesInClusters=[]
    const clusters=lineInfo
      .filter(c=>c.type== 'cluster')
      .reduce((clusterArray,cluster)=>{
        let mappedNodes=cluster.nodes.reduce((nodesObj,n)=>{
          if(nodes[n]) {
            nodesInClusters.push(n)
            return {...nodesObj,[n]:nodes[n]}
          }
          const cluster=clusterArray.find(c=>c.id==n)
          if(cluster) {
            return {...nodesObj,[n]:cluster}
          }
          return {...nodesObj,[n]:n}
        },{})
        return [
          ...clusterArray.filter(c=>!cluster.nodes.includes(c.id)),
          {
            id:cluster.id,
            opts:{clusterLabelPos:parserOpts.clusterLabelPos || 'top',...cluster.opts,label:cluster.label},
            nodes:mappedNodes
          }
        ]
      },[])
      .reduce((accum,curr,index)=>({
        ...accum,
        [curr.id||`__parserGroup${index}`]:curr
      }),{})
    
    nodesInClusters.forEach(n=>delete nodes[n]) //delete all the nested nodes from the root of the tree
    
    function unStringValue(valString) {
      if(typeof valString != 'string') return valString
      if(['true','false'].includes(valString.trim().toLowerCase())) return valString.trim().toLowerCase() === 'true'
      if(/^\d+\.?\d*$/.test(valString.trim())) return parseFloat(valString)
      return valString
    }

    const nodeOptions=lineInfo.filter(n=>n.type=='nodeOptions').reduce((accum,curr)=>Object.assign(accum,curr),{})
    delete nodeOptions.type

    return {
      nodes:{
        ...nodes,
        ...clusters
      },
      edges,
      ghostEdgeFlags,
      options:{
        ...parserOpts,
        graphOpts,
        urls
      },
      nodeOptions
    }
  }
  
  function parseLine(line) {
    return findNodeOptions(line) || findOption(line) || findCluster(line) || findNodeAndEdge(line)
  }
  function findNodeOptions(line) {
    const nodeOptRx=/^\s*<([^>]+)>\s*(\s*\([^\)]+\))/
    const rxResult=nodeOptRx.exec(line)
    
    if(rxResult) {
      const nodeOpts=getOptsFromSegment(rxResult[2])
      return rxResult[1].split(',').reduce((accum,curr)=>Object.assign(accum,{[curr]:nodeOpts}),{type:'nodeOptions'})
    }
  }
  function findOption(line) {
    const optionRx=/^\s*([^:}{]+)\s*:(?!\/\/)\s*([^:]+)/
    const rxResult=optionRx.exec(line)
    if(rxResult) return {key:rxResult[1],value:rxResult[2],type:'option'}
  }

  function getOptsFromSegment(segment) {
    return segment.replace('(','')
    .replace(')','')
    .split(',')
    .filter(opt=>opt.indexOf(':')>-1)
    .reduce((accum,curr)=>Object.assign(accum,{[curr.split(':')[0].trim()]:curr.split(':')[1].trim()}),{})
  }

  function findCluster(line) {
    const clusterRx=/^\s*([-_a-zA-Z0-9]+)?\s*\{([^}]+)\}\s*(\[([^\]]+)\])?(\s*\([^\)]+\))?/
    const rxResult=clusterRx.exec(line)

    if(rxResult) return {
      id:rxResult[1],
      nodes:rxResult[2].split(','),
      label:rxResult[4] ? rxResult[4].replace(/\\n/g,'\n') : '',
      opts:rxResult[5] ? getOptsFromSegment(rxResult[5]) : {},
      type:'cluster'
    }
  }
  function findNodeAndEdge(line) {
    function getNode(nodeString) {
      const nodeRx=/\s*([^\[]+)\s*(\[[^\]]+\])?\s*(\([^\)]+\))?\s*/
      const nodeRxResult=nodeRx.exec(nodeString)
      if(nodeRxResult) return {
        id:nodeRxResult[1].trim(),
        label:stripBrackets(nodeRxResult[2]) || null,
        url:stripBrackets(nodeRxResult[3])
      }
    }

    const arrowSplits = line.split(/\s*[_-]>\s*(\[[^\]]+\])?\s*/)
    const ghostEdgeFlags=[]
    let arrow;
    const arrowRx=/\s*([_-])>\s*(\[[^\]]+\])?\s*/g
    while(arrow=arrowRx.exec(line)) {
      ghostEdgeFlags.push(arrow[1]=='_' ? true : false)
    }
    
    if(arrowSplits.length > 2) {
      const nodes=[]
      const edges=[]
      const ghostEdges=[]
      for(let i=0; i< arrowSplits.length-1; i=i+2) {
        const leftNode=getNode(arrowSplits[i])
        const rightNode=getNode(arrowSplits[i+2])
        nodes.push(leftNode)
        nodes.push(rightNode)
        arrowSplits[i+1] !== undefined ? edges.push([leftNode.id,rightNode.id,stripBrackets(arrowSplits[i+1])]) : edges.push([leftNode.id,rightNode.id])
      }
      return {nodes,edges,ghostEdgeFlags}
    } else {
      const node=getNode(line)
      return {
        nodes:[node]
      }
    }
    
  }
  /////////////////////////////////////////////////

  const lines=inputString.split('\n')
  const lineInfo=lines.filter(l=>l.trim().length>0).map(parseLine)
  return compileLines(lineInfo)
}
