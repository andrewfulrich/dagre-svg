//this is a WIP file, please ignore
function getLines(text) {
  return text.split('\n').map(s=>s.trim())
}
function getLongestLine(lines) {
  lines.sort((a,b)=>b.length-a.length)
  return lines[0]
}

function setNode(graph,label,id,opts) {
  const metrics=getTextMetrics(label)
  metrics.width+=opts.margin*2
  metrics.height+=opts.margin*2
  metrics.url=opts.urls[id]
  graph.setNode(id,{label,...metrics})
}
function setCluster(graph,cluster,id,opts) {
  const metrics=getTextMetrics(cluster.opts.label)
  graph.setNode(id,{...cluster.opts,isCluster:true,...metrics})
  Object.keys(cluster.nodes).forEach(node=>setNode(graph,cluster.nodes[node],node,opts))
  Object.keys(cluster.nodes).forEach(node=>graph.setParent(node,id))
}
function createGraph(nodes,edges,optionsRaw={}) {
  const opts={
    margin:0,
    nodeColor:"#FFF",
    nodeBorder:false,
    edgeColor:"#000",
    edgeOpacity:1,
    hasArrows:true,
    edgeWidth:1,
    arrowSize:6,
    boldFirstLine:false,
    urls:{},
    ...optionsRaw,
    graphOpts:{
      marginx:0,
      marginy:0,
      ...optionsRaw.graphOpts
    }
  }
  var g = new dagre.graphlib.Graph({compound:true});
  // Set an object for the graph label
  g.setGraph(opts.graphOpts);
  // Default to assigning a new object as a label for each new edge.
  g.setDefaultEdgeLabel(function() { return {}; });
  Object.keys(nodes).forEach(node=>typeof nodes[node] == 'string' ? setNode(g,nodes[node],node,opts) : setCluster(g,nodes[node],node,opts))
  edges.forEach(edge=>g.setEdge(edge[0],edge[1],{label:edge[2],from:edge[0],to:edge[1],...getTextMetrics(edge[2])}))
  dagre.layout(g);
  //g.graph().width and g.graph().height cuts off edges, so get the true graph width and height from the edges if necessary
  const maxEdgeX=Math.max(...g.edges().map(e=>Math.max(...g.edge(e).points.map(p=>p.x))))
  const maxEdgeY=Math.max(...g.edges().map(e=>Math.max(...g.edge(e).points.map(p=>p.y))))
  const svg = createSvg(Math.max(g.graph().width+(opts.nodeBorder?1:0),maxEdgeX+opts.graphOpts.marginx+opts.edgeWidth),Math.max(g.graph().height+(opts.nodeBorder?1:0),maxEdgeY+opts.graphOpts.marginy+opts.edgeWidth),opts)
  
  renderLayout(g,svg,opts)
  return svg
}

const nodes={
  kspacey:'This is really cool!\nSpacey',
  swilliams:'Saul Williams',
  bpitt:"Brad Pitt",
  hford:"Harrison Ford",
  lwilson:"Luke Wilson",
  kbacon:"Kevin Bacon",
  group1:{
    opts:{
      label:'Stuff and Things',
      clusterLabelPos:'top'
    },
    nodes:{
      'stuff':"Stuff",
      'things':'things'
    }
  }
}
const edges=[
  ['kspacey','swilliams','my label'],
  ['swilliams','kbacon','another label'],
  ['bpitt','kbacon'],
  ['hford','lwilson'],
  ['lwilson','kbacon','move over?'],
  ['kbacon','lwilson','will it'],
  ['lwilson','kspacey'],
  ['stuff','kspacey'],
  ['things','bpitt'],
  ['stuff','things']
]
const options={
  margin:6,
  nodeColor:'#00F600',
  nodeBorder:'#000',
  boldFirstLine:true,
  hasArrows:true,
  edgeWidth:2,
  arrowSize:3,
  edgeOpacity:0.6,
  graphOpts:{
    // ranker:'tight-tree',
    ranksep:10,
    nodesep:20,
    rankdir:'LR',
    align:'UL'
  },
  urls:{
    kspacey:'http://www.example.com',
    stuff:'https://google.com'
  }
}
const svg = createGraph(nodes,edges,options)
saveSvg(svg)

function createSvg(width,height,opts) {
  return `<svg xmlns="http://www.w3.org/2000/svg"
  width=${width} height=${height}
  >
  <defs>
    <marker id="arrow" viewBox="0 0 10 10" refX=5 refY=5
    markerWidth=${opts.arrowSize}
    markerHeight=${opts.arrowSize}
    orient="auto-start-reverse" markerUnits="strokeWidth">
      <path d="M 0 0 L 10 5 L 0 10 z"></path>
    </marker>
  </defs>`
}
//convert x and y from center to top left corner:
function getCornerXY(node) {
	return {x:node.x-node.width/2,y:node.y-node.height/2}
}

function createTextRect(node, svg, opts) {
  function getRectAttrs() {
    if(node.isCluster) {
      return `fill="transparent" stroke=${opts.edgeColor}`
    } 
    else {
      return `fill=${opts.nodeColor}
      ${opts.nodeBorder ? `stroke=${opts.nodeBorder}` : ''}`
    }
  }
  const corner=getCornerXY(node)

  return `${node.url? `<a href=${node.url}>`:''}
  <rect width=${node.width} height=${node.height} ${getRectAttrs()}
  x=${corner.x} y=${corner.y} rx=3></rect>
  <text x=${node.x} y=${corner.y+node.drop+opts.margin} text-anchor="middle">
    ${getLines(node.label).map((line,i)=>`<tspan dy=${node.lineHeight*i} x=${node.x}
    ${(i==0 && opts.boldFirstLine ? 'font-weight="bold"' : '')}>${line}</tspan>`).join('')}
  </text>${node.url ? '</a>' : ''}
  `
}


function renderLayout(layout,svg,opts) {
	layout.nodes().forEach((node)=>{
    //layout.node(node) == {"label":"Kevin Spacey","width":144,"height":100,"x":80,"y":50}
    createTextRect(layout.node(node),svg,opts)
  })
  layout.edges().forEach((edge)=>{
    //layout.edge(edge) {"points":[{"x":80,"y":100},{"x":80,"y":125},{"x":80,"y":150}]}
    const edgeInfo=layout.edge(edge)
    if(edgeInfo.label) {
      createEdgeLabel(layout.edge(edgeInfo.from,edgeInfo.to),edgeInfo.label,svg)
    }
    createEdge(layout.edge(edge),svg,opts)
  })
}

function createEdge(edge,svg,opts) {
	const ns = 'http://www.w3.org/2000/svg'
  const path = document.createElementNS(ns, 'path')
  path.setAttribute('stroke',opts.edgeColor)
  path.setAttribute('stroke-width',opts.edgeWidth)
  path.setAttribute('fill','transparent')
  path.setAttribute('stroke-opacity',opts.edgeOpacity)
  path.setAttribute('d','M '+edge.points.map(p=>`${p.x},${p.y}`).join(' '))
  if(opts.hasArrows) path.setAttribute('marker-end','url(#arrow)');
  svg.appendChild(path)
}

function createEdgeLabel(coords,label,svg) {
  const ns = 'http://www.w3.org/2000/svg'
  const text=document.createElementNS(ns,'text')
  text.textContent=label
  text.setAttribute('x',coords.x)
  text.setAttribute('y',coords.y)
  text.setAttribute('text-anchor','middle')
  text.setAttribute('dominant-baseline','central')
  svg.appendChild(text)
}

function getTextMetrics(text) {
    if(!text) return {}
    // re-use canvas object for better performance
    const longestLine=getLongestLine(getLines(text))
    const numLines=getLines(text).length
    const lineHeight=16

    return {
      width:longestLine.length*5,
      height:lineHeight*numLines,
      drop:lineHeight,
      lineHeight
    };
}

/**
 * 
 * @param {*} svgEl 
 * @param {*} name 
 * 
 * @see https://stackoverflow.com/questions/23218174/how-do-i-save-export-an-svg-file-after-creating-an-svg-with-d3-js-ie-safari-an
 */
function saveSvg(svgEl, name) {
  var svgData = svgEl.outerHTML;
  var preface = '<?xml version="1.0" standalone="no"?>\r\n';
  var svgBlob = new Blob([preface, svgData], {type:"image/svg+xml;charset=utf-8"});
  var svgUrl = URL.createObjectURL(svgBlob);
  var downloadLink = document.getElementById('download')
  downloadLink.href = svgUrl;
  downloadLink.addEventListener('click',()=>{
    downloadLink.download = document.getElementById('svgName').value;
  })
}