/**
 * @license
 * dagre-svg
 * 
 * ISC License
 * Copyright (c) 2021 Andrew Ulrich
 * 
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


/**
 * creates an svg graph from a set of JSON objects
 * @param {*} nodes an object which maps nodes to labels
 * @param {*} edges an array of arrays. First two elements in the inner array are the nodes that the edge connects, and the third, optional element is the edge label
 * @param {*} options an object which maps options to option values, see the readme for all the options
 * @param {*} nodeOptions an object which maps specific node ids to custom options for that node
 * @param {*} ghostEdgeFlags each element corresponds to an edge in the edges array at the same index, flagging it as invisible or not
 */
function createGraph(nodes,edges,optionsRaw={},nodeOptions={},ghostEdgeFlags=[]) {
  function autoWrap(line,wrapLength) {
    if(wrapLength && line.length > wrapLength) {
      let lastSpaceIndex=line.substring(0,wrapLength).lastIndexOf(' ')
      if(lastSpaceIndex==-1) lastSpaceIndex=line.substring(wrapLength).indexOf(' ')
      if(lastSpaceIndex==-1) return [line]
      return [line.substring(0,lastSpaceIndex),...autoWrap(line.substring(lastSpaceIndex+1),wrapLength)]
    } else return [line]
  }
  function getLines(text,wrapLength) {
    return text.split('\n')
      .map(s=>s.trim())
      .reduce((accum,curr)=>[...accum,...autoWrap(curr,wrapLength)],[])
  }
  function getLongestLine(lines) {
    lines.sort((a,b)=>b.length-a.length)
    return lines[0]
  }
  
  function setNode(graph,label,id,opts) {
    const metrics=getTextMetrics(label,opts.autoWrapLength)
    metrics.width+=opts.margin*2
    metrics.height+=opts.margin*2
    metrics.url=opts.urls[id]
    metrics.id=id
    graph.setNode(id,{label,...metrics})
  }
  function setCluster(graph,cluster,id,opts,clusterOpts) {
    clusterOpts[id]=Object.assign({},opts,cluster.opts)
    const metrics=getTextMetrics(cluster.opts.label.length > 0 ? cluster.opts.label : ' ',clusterOpts.autoWrapLength)
    graph.setNode(id,{id,...cluster.opts,isCluster:true,...metrics})
    Object.keys(cluster.nodes).forEach(node=>clusterOpts[node]=clusterOpts[id])
    Object.keys(cluster.nodes).forEach(node=>typeof cluster.nodes[node]=='string' ? setNode(graph,cluster.nodes[node],node,clusterOpts[id]):setCluster(graph,cluster.nodes[node],node,clusterOpts[id],clusterOpts))
    Object.keys(cluster.nodes).forEach(node=>graph.setParent(node,id))
  }

  function createSvg(width,height,opts) {
    const ns = 'http://www.w3.org/2000/svg'
    const svg = document.createElementNS(ns, 'svg')
    svg.setAttribute('width', width)
    svg.setAttribute('height', height)
    svg.setAttribute('xmlns',ns)
  
    const arrow = document.createElementNS(ns,'marker')
    arrow.setAttribute('id','arrow')
    arrow.setAttribute('viewBox','0 0 10 10')
    arrow.setAttribute('refX',5)
    arrow.setAttribute('refY',5)
    arrow.setAttribute('markerWidth',opts.arrowSize)
    arrow.setAttribute('markerHeight',opts.arrowSize)
    arrow.setAttribute('orient','auto-start-reverse')
    arrow.setAttribute('markerUnits','strokeWidth')
    const path=document.createElementNS(ns,'path')
    path.setAttribute('d','M 0 0 L 10 5 L 0 10 z')
    arrow.appendChild(path)
    const defs=document.createElementNS(ns,'defs')
    defs.appendChild(arrow)
    svg.appendChild(defs)
    return svg
  }
  //convert x and y from center to top left corner:
  function getCornerXY(node) {
    return {x:node.x-node.width/2,y:node.y-node.height/2}
  }
  
  function createTextRect(node, svg, opts) {
    const ns = 'http://www.w3.org/2000/svg'
    const rect = document.createElementNS(ns, 'rect')
    rect.setAttribute('width', node.width)
    rect.setAttribute('height', node.height)
    if(node.isCluster) {
      rect.setAttribute('fill','none')
      rect.setAttribute('stroke',opts.edgeColor)
    } 
    else {
      rect.setAttribute('fill', opts.nodeColor)
      if(opts.nodeBorder) rect.setAttribute('stroke',opts.nodeBorder)
    }
    
    const corner=getCornerXY(node)
    rect.setAttribute('x',corner.x)
    rect.setAttribute('y',corner.y)
    switch(opts.cornerRadius) {
      case 'ellipse':
        rect.setAttribute('rx',node.width/2)
        rect.setAttribute('ry',node.height/2)
        break;
      case 'pill':
        rect.setAttribute('rx',node.width/2)
        rect.setAttribute('ry',(node.width+node.height)/20)
        break;
      default:
        rect.setAttribute('rx',isNaN(opts.cornerRadius) ? 0 : parseInt(opts.cornerRadius))
        rect.setAttribute('ry',isNaN(opts.cornerRadius) ? 0 : parseInt(opts.cornerRadius))
        break;
    }
    let idText;
    if(opts.showIds) {
      idText=document.createElementNS(ns, 'text')
      idText.setAttribute('x',corner.x)
      idText.setAttribute('y',corner.y+parseInt(opts.margin))
      idText.textContent=node.id
    }
    const text = document.createElementNS(ns, 'text')
    text.setAttribute('x',node.x)
    text.setAttribute('y',corner.y+node.drop+parseInt(opts.margin))
    text.setAttribute('text-anchor','middle')
    getLines(node.label,opts.autoWrapLength).forEach((line,i)=>{
      const tspan=document.createElementNS(ns,'tspan')
      tspan.textContent=line
      tspan.setAttribute('dy',i && node.lineHeight)
      tspan.setAttribute('x',node.x)
      if(i==0 && opts.boldFirstLine) tspan.setAttribute('font-weight','bold')
      
      text.appendChild(tspan)
    })
    let appendToThis=svg
    if(node.url) {
      const a = document.createElementNS(ns,'a')
      a.setAttribute('href',node.url)
      svg.appendChild(a)
    }
    appendToThis.appendChild(rect)
    appendToThis.appendChild(text)
    if(idText) appendToThis.appendChild(idText)
  }
  
  function renderLayout(layout,svg,opts,clusterOpts,nodeOpts) {
    layout.nodes().forEach((node)=>{
      createTextRect(layout.node(node),svg,getOptsForNode(clusterOpts[node] || opts,node,nodeOpts))
    })
    layout.edges().forEach((edge,index)=>{
      const edgeInfo=layout.edge(edge)
      
      if(edgeInfo.label) {
        createEdgeLabel(layout.edge(edgeInfo.from,edgeInfo.to),edgeInfo.label,svg)
      }

      createEdge(layout.edge(edge),svg,opts,ghostEdgeFlags[index])
    })
  }
  
  function createEdge(edge,svg,opts,isGhostEdge) {
    const ns = 'http://www.w3.org/2000/svg'
    const path = document.createElementNS(ns, 'path')
    if(!isGhostEdge) {
      path.setAttribute('stroke',opts.edgeColor)
      path.setAttribute('stroke-width',opts.edgeWidth)
      path.setAttribute('stroke-opacity',opts.edgeOpacity)
    }
    path.setAttribute('fill','none')
    path.setAttribute('d','M '+edge.points.map(p=>`${p.x},${p.y}`).join(' '))
    if(opts.hasArrows && !isGhostEdge) path.setAttribute('marker-end','url(#arrow)');
    svg.appendChild(path)
  }
  
  function createEdgeLabel(coords,label,svg) {
    const ns = 'http://www.w3.org/2000/svg'
    const text=document.createElementNS(ns,'text')
    text.textContent=label
    text.setAttribute('x',coords.x)
    text.setAttribute('y',coords.y)
    text.setAttribute('text-anchor','middle')
    text.setAttribute('dominant-baseline','central')
    svg.appendChild(text)
  }
  
  /**
   * Uses canvas.measureText to compute and return the metrics of the given text of given font in pixels.
   * 
   * @param {String} text The text to be rendered.
   * 
   * @see https://stackoverflow.com/questions/118241/calculate-text-width-with-javascript/21015393#21015393
   */
  function getTextMetrics(text,autoWrapLength) {
      if(!text) return {}
      // re-use canvas object for better performance
      const longestLine=getLongestLine(getLines(text,autoWrapLength))
      const numLines=getLines(text,autoWrapLength).length
      var canvas = getTextMetrics.canvas || (getTextMetrics.canvas = document.createElement("canvas"));
      var context = canvas.getContext("2d");
      // const el=document.createElementNS('http://www.w3.org/2000/svg','text')
      // el.textContent="test"
      // el.setAttribute('class',cssClass)
      const el=document.body //TODO: this is going off the body style- maybe use a rendered sample text node instead
      context.font = `${window.getComputedStyle(el, null).getPropertyValue('font-size')} ${window.getComputedStyle(el,null).getPropertyValue('font-family')}`;
      
      var metrics = context.measureText(longestLine);
      const extraLineHeight=2 //so the lines aren't so squished together
      return {
        width:metrics.width,
        height:(metrics.actualBoundingBoxAscent+metrics.actualBoundingBoxDescent +extraLineHeight)*numLines,
        drop:metrics.actualBoundingBoxAscent,
        lineHeight:metrics.actualBoundingBoxAscent+metrics.actualBoundingBoxDescent+extraLineHeight
      };
  }
  
  function getOptsForNode(opts,node,nodeOpts) {
    if(node=='bpitt') 
      console.log(node,nodeOpts,Object.assign({},opts,nodeOpts[node]))
    return nodeOpts[node] ? Object.assign({},opts,nodeOpts[node]) : opts;
  }

  const opts={
    margin:0,
    autoWrapLength:50, //can be either an integer or false
    nodeColor:"#FFF",
    nodeBorder:false, //can be either an integer or false
    edgeColor:"#000",
    edgeOpacity:1,
    hasArrows:true,
    cornerRadius:3,//can also be ellipse or pill
    edgeWidth:1,
    arrowSize:6,
    boldFirstLine:false,
    urls:{},
    showIds:false,
    ...optionsRaw,
    graphOpts:{
      marginx:0,
      marginy:0,
      ...optionsRaw.graphOpts
    }
  }
  var g = new dagre.graphlib.Graph({compound:true});
  // Set an object for the graph label
  g.setGraph(opts.graphOpts);
  // Default to assigning a new object as a label for each new edge.
  g.setDefaultEdgeLabel(function() { return {}; });
  const clusterOpts=[]
  Object.keys(nodes).forEach(node=>typeof nodes[node] == 'string' ? setNode(g,nodes[node],node,opts) : setCluster(g,nodes[node],node,opts,clusterOpts))
  edges.forEach(edge=>g.setEdge(edge[0],edge[1],{label:edge[2],from:edge[0],to:edge[1],...getTextMetrics(edge[2])}))
  dagre.layout(g);
  //g.graph().width and g.graph().height cuts off edges, so get the true graph width and height from the edges if necessary
  const maxEdgeX=Math.max(...g.edges().map(e=>Math.max(...g.edge(e).points.map(p=>p.x))))
  const maxEdgeY=Math.max(...g.edges().map(e=>Math.max(...g.edge(e).points.map(p=>p.y))))
  const svg = createSvg(Math.max(g.graph().width+(opts.nodeBorder?1:0),maxEdgeX+opts.graphOpts.marginx+opts.edgeWidth),Math.max(g.graph().height+(opts.nodeBorder?1:0),maxEdgeY+opts.graphOpts.marginy+opts.edgeWidth),opts)
  
  renderLayout(g,svg,opts,clusterOpts,nodeOptions)
  return svg
}


